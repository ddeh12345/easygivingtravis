//
//  AmountCollectionViewCell.swift
//  EasyGivings
//
//  Created by Amalchithra on 18/04/19.
//  Copyright © 2019 Amalchithra. All rights reserved.
//

import UIKit

class AmountCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var viewAmount: UIView!
    @IBOutlet weak var lblAmount: UILabel!
    
   
    override var isSelected: Bool {
        didSet {
            if self.isSelected {
            viewAmount.backgroundColor = UIColor.lightGray
            viewAmount.layer.borderColor = UIColor.darkGray.cgColor
            viewAmount.layer.borderWidth = 1
            } else {
                viewAmount.backgroundColor = #colorLiteral(red: 0.6039215686, green: 0.6039215686, blue: 0.6039215686, alpha: 0.1709920805)
                viewAmount.layer.borderColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 0)
                viewAmount.layer.borderWidth = 1
            }
        }
    }
}

