import UIKit
import Alamofire
import AlamofireObjectMapper
import MBProgressHUD

class NewViewController: UIViewController,Delegate,msgDelegate {
    
    @IBOutlet weak var collectnViewTopConstrnt: NSLayoutConstraint!
    @IBOutlet weak var btnLogout: UIButton!
    @IBOutlet weak var btnSearch: UIButton!
    @IBOutlet weak var btnPlus: UIButton!
    @IBOutlet weak var txtFinalAmount: UITextField!
    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var viewOtherOrgList: ViewWithShadow!
    @IBOutlet weak var viewOfferDonatn: UIView!
    @IBOutlet weak var viewSecTopConstarint: NSLayoutConstraint!
    @IBOutlet weak var viewOffering: UIStackView!
    @IBOutlet weak var viewDonation: UIView!
    @IBOutlet weak var lblSubOrg: UILabel!
    @IBOutlet weak var btnDonation: UIButton!
    @IBOutlet weak var btnOffering: UIButton!
    @IBOutlet weak var lblFinalAmount: UILabel!
    @IBOutlet weak var viewThird: UIView!
    @IBOutlet weak var viewSecond: UIView!
    @IBOutlet weak var viewFirst: UIView!
    @IBOutlet weak var viewImage: UIView!
    @IBOutlet weak var lblOrgAddress: UILabel!
    @IBOutlet weak var lblOrgName: UILabel!
    @IBOutlet weak var lblLastAmt: UILabel!
    @IBOutlet weak var lblFirstAmt: UILabel!
    @IBOutlet weak var lblMidAmount: UILabel!
    @IBOutlet weak var tblOtherOrgList: UITableView!

    var data: searchResponse! = nil
    var amountArray = [String]()
    var currentArrayIndex = 0
    var j: Int!
    var name: String!
    var address: String!
    var orgDetailArr = [getOrgDetailResponse]()
    var orgID: String!
    var orgOtherArr: [String] = []
    var orgOtherFeeArr: [String] = []
    var isOffering: Bool!
    var txtmessage: String!
    var buttonTappedCount : Int = 0
    //var selectedId: String!

    override func viewDidLoad() {
        super.viewDidLoad()
        btnPlus.contentMode = .scaleAspectFit
        btnLogout.contentMode = .scaleAspectFit
        btnSearch.contentMode = .scaleAspectFit
        viewFirst.isHidden = true
        viewSecond.isHidden = true
        viewThird.isHidden = true
        lblFinalAmount.isHidden = true
//        if isOffering == false {
//        txtFinalAmount.text = "20"
//        }
        //let parameters: Parameters = ["organizationid" : orgID]
       // getOrganizationDetailapicall(paramDict: parameters, Url: getOrgDetailApiUrl)
        
        //viewOfferNDonation.layer.masksToBounds = true
        txtmessage = ""
        isOffering = true
        viewOfferDonatn.layer.shadowColor = #colorLiteral(red: 0.6642242074, green: 0.6642400622, blue: 0.6642315388, alpha: 1)
        viewOfferDonatn.layer.shadowOpacity = 1
        viewOfferDonatn.layer.shadowOffset = CGSize.zero
        viewOfferDonatn.layer.shadowRadius = 3
        
        viewOtherOrgList.isHidden = true
        scrollView.isScrollEnabled = true
        lblOrgName.text = name
        lblOrgAddress.text = address
        j = 0
        for i in 0 ... 100 {
            j = j + 10
            amountArray.append(String(j))
            
        }
        
        let leftSwipe = UISwipeGestureRecognizer(target: self, action: #selector(NewViewController.handleSwipes(sender:)))
        
        let rightSwipe = UISwipeGestureRecognizer(target: self, action: #selector(NewViewController.handleSwipes(sender:)))
        
        leftSwipe.direction = .left
        rightSwipe.direction = .right
        
        view.addGestureRecognizer(leftSwipe)
        view.addGestureRecognizer(rightSwipe)
//        lblFirstAmt.text = amountArray[currentArrayIndex]
//        lblFinalAmount.text = amountArray[currentArrayIndex + 1]
//        lblMidAmount.text = amountArray[currentArrayIndex + 1]
//        lblLastAmt.text = amountArray[currentArrayIndex + 2]
        btnDonation.layer.cornerRadius = btnDonation.frame.height/2
        btnOffering.layer.cornerRadius = btnOffering.frame.height/2
        btnOffering.backgroundColor = #colorLiteral(red: 0.3764705882, green: 0.7411764706, blue: 0.2862745098, alpha: 1)
        btnDonation.backgroundColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
        btnOffering.setTitleColor(#colorLiteral(red: 1, green: 1, blue: 1, alpha: 1), for: .normal)
        btnDonation.setTitleColor(#colorLiteral(red: 0, green: 0, blue: 0, alpha: 1), for: .normal)
        viewOffering.isHidden = false
        viewDonation.isHidden = true

    }
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        scrollView.contentSize = CGSize(width: self.view.frame.size.width, height: 800)
        viewFirst.layer.cornerRadius = self.viewFirst.frame.height/2
        viewFirst.layer.masksToBounds = true
        viewSecond.layer.cornerRadius = self.viewSecond.frame.height/2
        viewSecond.layer.masksToBounds = true
        viewThird.layer.cornerRadius = self.viewThird.frame.height/2
        viewThird.layer.masksToBounds = true
        viewImage.layer.masksToBounds = true
        viewImage.layer.cornerRadius = self.viewImage.frame.height/2
        viewSecond.layer.borderColor = UIColor.darkGray.cgColor
        viewSecond.layer.borderWidth = 1
        viewOfferDonatn.layer.cornerRadius = viewOfferDonatn.frame.height/2

        
    }
   
    
    //MARK:- Swipe function to set amount
    @objc func handleSwipes(sender: UISwipeGestureRecognizer) {
        
        if sender.direction == .left {
            if(currentArrayIndex < amountArray.count - 1)
            {
                currentArrayIndex += 1
                let indexPath = IndexPath(item: currentArrayIndex, section: 0)
                lblMidAmount.text = amountArray[indexPath.row]
                //lblFinalAmount.text = amountArray[indexPath.row]
                txtFinalAmount.text = amountArray[indexPath.row]
                if indexPath.row > 0 {
                    lblFirstAmt.text = amountArray[indexPath.row - 1]
                }
                lblLastAmt.text = amountArray[indexPath.row + 1]
            }
        }
        if sender.direction == .right {
            if(currentArrayIndex > 0)
            {
                currentArrayIndex -= 1
                if currentArrayIndex == -1
                {
                    currentArrayIndex = 0
                    let indexPath = IndexPath(item: currentArrayIndex, section: 0)
                    lblMidAmount.text = amountArray[indexPath.row]
                    //lblFinalAmount.text = amountArray[indexPath.row]
                    txtFinalAmount.text = amountArray[indexPath.row]
                     if indexPath.row > 0 {
                        lblFirstAmt.text = amountArray[indexPath.row - 1]
                    }
                    lblLastAmt.text = amountArray[indexPath.row + 1]
                }
                else {
                    let indexPath = IndexPath(item: currentArrayIndex, section: 0)
                    lblMidAmount.text = amountArray[indexPath.row]
                    //lblFinalAmount.text = amountArray[indexPath.row]
                    txtFinalAmount.text = amountArray[indexPath.row]
                     if indexPath.row > 0 {
                    lblFirstAmt.text = amountArray[indexPath.row - 1]
                    }
                    lblLastAmt.text = amountArray[indexPath.row + 1]
                
                }
            }
        }
    }
   
//    func didSelectOrg(_ result: String) {
//        print(result)
//        orgID = result
//        print(orgID)
//    }
    
    func didSelectOrg(_ result: String, _ name: String, _ address: String) {
        print(result)
        orgID = result
        lblOrgName.text = name
        lblOrgAddress.text = address
    }
    func message(_ message: String) {
        txtmessage = message
        
    }
    
    //MARK:- Button Actions
    @IBAction func btnPersonalDetails(_ sender: Any) {
        let VC = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "PersonalDetailsViewController") as! PersonalDetailsViewController
        VC.delegate = self
        self.present(VC, animated: true, completion: nil)
        
    }
    
    @IBAction func btnBack(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    
    
    @IBAction func btnPlus(_ sender: Any) {
        
        if let url = URL(string: "http://register.easygivings.com") {
            UIApplication.shared.open(url, options: [:])
        }
        
    }
    @IBAction func btnLogout(_ sender: Any) {
        
        let uiAlert = UIAlertController(title: "Do you want to logout", message: "", preferredStyle: UIAlertController.Style.alert)
        self.present(uiAlert, animated: true, completion: nil)
        
        uiAlert.addAction(UIAlertAction(title: "Ok", style: .default, handler: { action in
            UserDefaults.standard.set(false, forKey: "isUserLoggedIn")
            UserDefaults.standard.synchronize()
            
            defaultvalue.removeObject(forKey: "fname")
            defaultvalue.removeObject(forKey: "lname")
            defaultvalue.removeObject(forKey: "email")
            defaultvalue.removeObject(forKey: "mobile")
            defaultvalue.removeObject(forKey: "address")
            defaultvalue.removeObject(forKey: "Message")
            defaultvalue.removeObject(forKey: "Addrss")

            let loginVC = self.storyboard?.instantiateViewController(withIdentifier: "LoginViewController") as! LoginViewController
            
            let appDel: AppDelegate = UIApplication.shared.delegate as! AppDelegate
            appDel.window?.rootViewController = loginVC
            
//            let VC = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "LoginViewController") as! LoginViewController
//
//            self.present(VC, animated: true, completion: nil)
        }))
        
        uiAlert.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: { action in
            
        }))
        
    }
    @IBAction func btnSelectsubOrg(_ sender: Any) {
        
        buttonTappedCount = buttonTappedCount+1
        if(buttonTappedCount % 2 == 0) {
            viewOtherOrgList.isHidden = true
            scrollView.isScrollEnabled = true
        } else {

            let parameters: Parameters = ["organizationid" : orgID]
            getOrganizationDetailapicall(paramDict: parameters, Url: getOrgDetailApiUrl)
            viewOtherOrgList.isHidden = false
            scrollView.isScrollEnabled = false
            orgOtherArr.removeAll()
        }
    }
    @IBAction func btnSearchAction(_ sender: Any) {
      
        let vc = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "SearchhViewController") as! SearchhViewController
        vc.fromHome = true
        vc.delegate = self
        self.present(vc, animated: true, completion: nil)
    }
    
    @IBAction func btnOneTymPayment(_ sender: Any) {
        Alert.showAlertView(withTitle: "", withMessage: "Coming soon")
    }
    
    @IBAction func btnNext(_ sender: Any) {
        if isOffering == true {
            if lblSubOrg.text != "Select" {
                let vc = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "PaymentMethodViewController") as! PaymentMethodViewController
                vc.data = data
                vc.orgID = orgID
                //vc.totalAmount = lblFinalAmount.text
                vc.totalAmount = txtFinalAmount.text
                vc.txtmessage = txtmessage
                self.present(vc, animated: true, completion: nil)
            } else {
                Alert.showAlertView(withTitle: "", withMessage: "Select Category")
            }
        } else {
            let vc = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "PaymentMethodViewController") as! PaymentMethodViewController
            vc.data = data
            vc.orgID = orgID
            vc.totalAmount = txtFinalAmount.text
            vc.txtmessage = txtmessage
            self.present(vc, animated: true, completion: nil)
        }
    }
    @IBAction func btnDonation(_ sender: Any) {
        txtFinalAmount.text = ""
        txtFinalAmount.isUserInteractionEnabled = false
        btnDonation.backgroundColor = #colorLiteral(red: 0.3764705882, green: 0.7411764706, blue: 0.2862745098, alpha: 1)
        btnOffering.backgroundColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
        btnDonation.setTitleColor(#colorLiteral(red: 1, green: 1, blue: 1, alpha: 1), for: .normal)
        btnOffering.setTitleColor(#colorLiteral(red: 0, green: 0, blue: 0, alpha: 1), for: .normal)
        viewOffering.isHidden = true
        viewDonation.isHidden = false
        isOffering = false
        viewSecTopConstarint.constant = -40
        collectnViewTopConstrnt.constant = -20

    }
    @IBAction func btnOffering(_ sender: Any) {
        if txtFinalAmount.text == "0" || txtFinalAmount.text == "" {
        txtFinalAmount.isUserInteractionEnabled = true
        }
        btnOffering.backgroundColor = #colorLiteral(red: 0.3764705882, green: 0.7411764706, blue: 0.2862745098, alpha: 1)
        btnDonation.backgroundColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
        btnOffering.setTitleColor(#colorLiteral(red: 1, green: 1, blue: 1, alpha: 1), for: .normal)
        btnDonation.setTitleColor(#colorLiteral(red: 0, green: 0, blue: 0, alpha: 1), for: .normal)
        viewOffering.isHidden = false
        viewDonation.isHidden = true
        isOffering = true
        viewSecTopConstarint.constant = 40
        collectnViewTopConstrnt.constant = 10

    }
//    override var preferredStatusBarStyle: UIStatusBarStyle {
//        return .lightContent
//    }
}

//MARK:- TableView dataSourse & delegate methods
extension NewViewController: UITableViewDelegate,UITableViewDataSource {

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
            return orgOtherArr.count
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {

        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as! OrgListTableViewCell
            cell.lblOrgName.text = orgOtherArr[indexPath.row]
        return cell
    }

    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let cell = tableView.cellForRow(at: indexPath) as! OrgListTableViewCell
        
            let selectedName = orgOtherArr[indexPath.row]
            //txtOtherSelect.text = selectedName
            //let selectFee = orgOtherFeeArr[indexPath.row]
           // txtPaymntAmt2.text = selectFee
           // txtPaymntAmt3.becomeFirstResponder()
            lblSubOrg.text = selectedName
            txtFinalAmount.text = orgOtherFeeArr[indexPath.row]
            viewOtherOrgList.isHidden = true
            scrollView.isScrollEnabled = true
        
    }

}

//MARK:- collectionview dataSourse and Delegate Methods
extension NewViewController: UICollectionViewDelegate,UICollectionViewDataSource {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return amountArray.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "Cell", for: indexPath) as! AmountCollectionViewCell
        
        cell.lblAmount.text = amountArray[indexPath.item]
        cell.viewAmount.layer.cornerRadius = cell.viewAmount.frame.height/2
        cell.viewAmount.layer.masksToBounds = true
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        txtFinalAmount.text = ""
        txtFinalAmount.text = amountArray[indexPath.item]

    }
    
    
}
//MARK:- Api methods

extension NewViewController {

    func getOrganizationDetailapicall(paramDict:[String:Any],Url: String) {
        self.showHUD()
        let url = URL(string: Url)
        var status = ""
        var statusCode = 0
        let queue = DispatchQueue(label: "com.test.api", qos: .background, attributes: .concurrent)
        Alamofire.request(url!, method: .post, parameters: paramDict, encoding: JSONEncoding.default, headers: headers)
            .responseJSON(completionHandler: { (response) in
                print(response.result.value!)
            })
            .validate(statusCode: 200..<600)
            .responseObject(queue: queue){ (response: DataResponse<UrlResponse>) in
                DispatchQueue.main.async {
                    guard response.result.isSuccess else {
                        print("Error while fetching remote rooms")
                        print(response.error.debugDescription)
                        status = (response.error?.localizedDescription)!
                        self.dismissHUD()
                        Alert.showAlertView(withTitle: status, withMessage: "")
                        return
                    }
                    let requestResponse = response.result.value
                    status = (requestResponse?.status)!
                    statusCode = (requestResponse?.statusCode)!
                }
            }
            .responseObject(queue: queue, keyPath: "data") { (response: DataResponse<getOrgDetailResponse>) in
                
                DispatchQueue.main.async {
                    if statusCode == 200 && status == "Success" {
                        print("Success")
                        self.dismissHUD()
                        
                        let orgResponse = response.result.value
                        
                        print(response.result.value!)
                        
                        if let club = (orgResponse?.Club)
                        {
                            
                            
                            print(club[0])
                            
                            for value in club {
                                print(value)
                                let dict: [String : String]!
                                dict = value as! [String : String]
                                print("dict",dict)
                                var strKey: String!
                                var strValue: String!
                                for (key,value) in dict {
                                    print(key)
                                    strKey = key
                                    strValue = value
                                    print(value)
                                    
                                }
                                self.orgOtherArr.append(strKey)
                                self.orgOtherFeeArr.append(strValue)
                            }
                            
                            print(self.orgOtherArr)
                            
                            
                            self.tblOtherOrgList.reloadData()
                        }
                        
                    } else {
                        if status == "" {
                            self.dismissHUD()
                            let parameters: Parameters = ["organizationid" : self.orgID]
                            self.getOrganizationDetailapicall(paramDict: parameters, Url: getOrgDetailApiUrl)
                            
                        } else {
                            self.dismissHUD()
                            Alert.showAlertView(withTitle: "Something went Wrong", withMessage: status)
                            self.viewOtherOrgList.isHidden = true
                            self.scrollView.isScrollEnabled = true
                        }
                    }
                }
        }
    }
    func showHUD() {
        let progressHUD = MBProgressHUD.showAdded(to: self.view, animated: true)
        progressHUD.label.text = "Loading"
    }
    
    func dismissHUD() {
        MBProgressHUD.hide(for: self.view, animated: true)
    }
    
}

