

import UIKit
import SafariServices
import Alamofire
import AlamofireObjectMapper
import BraintreeDropIn
import Braintree
import MBProgressHUD

class PaymentMethodViewController: UIViewController {

    @IBOutlet weak var viewMobilee: UIView!
    @IBOutlet weak var viewCash: UIView!
    @IBOutlet weak var viewBank: UIView!
    @IBOutlet weak var viewCredit: UIView!
    @IBOutlet weak var imgTopCurrency: UIImageView!
    @IBOutlet weak var lblAmount: UILabel!
    @IBOutlet weak var imgCash: UIImageView!
    @IBOutlet weak var imgMobileMoney: UIImageView!
    @IBOutlet weak var imgBank: UIImageView!
    @IBOutlet weak var imgCredit: UIImageView!
    @IBOutlet weak var tblCurrency: UITableView!
    @IBOutlet weak var imgCurrency: UIImageView!
    @IBOutlet weak var viewCurrency: UIView!
    var flagArray: Array = [UIImage]()
    var currencyArray: Array = [UIImage]()
    
    var data: searchResponse! = nil
   
    var braintreeClient: BTAPIClient?
    var clientID: String!
    
    var nounce: String!
    var isCountry: Bool!
    var isCurrency: Bool!
    var isPayment: Bool!
    var countryArr = [String]()
    var clientArr = [clientTokenResponse]()
    var transDic: [String : AnyObject]!
    var userID: String!
    var date: String!
    var totalAmount: String!
    var paymentType: String!
    var transID: String!
    var orgID: String!
    var buttonTappedCount : Int = 0
    var myPickerView : UIPickerView!
    var txtmessage: String!

   
    var paymentMode: String!
    var cardList: [String]!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        print(txtmessage)
        lblAmount.text = totalAmount
        viewCash.layer.cornerRadius = self.viewCash.frame.height/2
        viewCash.layer.masksToBounds = true
        viewMobilee.layer.cornerRadius = self.viewMobilee.frame.height/2
        viewMobilee.layer.masksToBounds = true
        viewBank.layer.cornerRadius = self.viewBank.frame.height/2
        viewBank.layer.masksToBounds = true
        viewCredit.layer.cornerRadius = self.viewCredit.frame.height/2
        viewCredit.layer.masksToBounds = true
        paymentMode = ""
        if let id = defaultvalue.value(forKey: "ID") as? String {
            userID = id
        }
        viewCurrency.layer.maskedCorners = [.layerMinXMinYCorner, .layerMaxXMinYCorner]
        viewCurrency.isHidden = true
        imgBank.isHidden = true
        imgCredit.isHidden = true
        imgMobileMoney.isHidden = true
        imgCash.isHidden = true
        let formatter = DateFormatter()
        formatter.dateFormat = "yyyy-MM-dd"
        let now = Date()
        let dateString = formatter.string(from:now)
        date = dateString
        flagArray = [UIImage(named: "aus")!, UIImage(named: "BRAZIL")!, UIImage(named: "britain")!, UIImage(named: "HONGKONG")!, UIImage(named: "HUNGARY")!, UIImage(named: "IND")!, UIImage(named: "isr")!, UIImage(named: "jap")!, UIImage(named: "malaysia")!, UIImage(named: "mexico")!, UIImage(named: "newzeland")!, UIImage(named: "norway")!, UIImage(named: "philipines")!, UIImage(named: "poland")!, UIImage(named: "russia")!, UIImage(named: "singapore")!, UIImage(named: "sweden")!, UIImage(named: "switzerland")!, UIImage(named: "tailand")!, UIImage(named: "taiwan")!, UIImage(named: "turkey")!, UIImage(named: "USA")!]
        currencyArray = [UIImage(named: "AUS DOLLARS")!, UIImage(named: "BRAZILIAN RIES")!, UIImage(named: "BRITISH POUNDS")!, UIImage(named: "HONGKONG DOLLARS")!, UIImage(named: "HUNGARIAN FORITIS")!, UIImage(named: "INDIAN RUPEE")!, UIImage(named: "ISRAIL SHAKEL")!, UIImage(named: "JAPAN YEN")!, UIImage(named: "MALASIYAN RINKET")!, UIImage(named: "MexicoCurrencyx")!, UIImage(named: "NEWZELAND DOLLARS ")!, UIImage(named: "PESO")!, UIImage(named: "PHILIPINESCurrncy")!, UIImage(named: "POLISH ZLOTY")!, UIImage(named: "RUSSIACurrency")!, UIImage(named: "SINGAPORE DOLLARS")!, UIImage(named: "SWEDISH KORNA")!, UIImage(named: "SWITZCurrency")!, UIImage(named: "THAI BHAT")!, UIImage(named: "TAIWAQN DOLLAR")!, UIImage(named: "TURKEYCurrency")!, UIImage(named: "US DOLLARS")!]
        
    }
//    override var preferredStatusBarStyle: UIStatusBarStyle {
//        return .lightContent
//    }
    //MARK:- Button Actions
    @IBAction func btnDismiss(_ sender: Any) {
        viewCurrency.isHidden = true

    }
    
    @IBAction func btnBack(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    @IBAction func btnCurrencySelect(_ sender: Any) {
        viewCurrency.isHidden = false
        
//        self.myPickerView = UIPickerView(frame:CGRect(x: 0, y: self.view.frame.size.height-250, width: self.view.frame.size.width, height: 250))
//        self.myPickerView.delegate = self
//        self.myPickerView.dataSource = self
//        self.myPickerView.backgroundColor = UIColor.white
//        self.view.addSubview(self.myPickerView)
        
    }
    
    @IBAction func btnCreditCard(_ sender: Any) {
        
        buttonTappedCount = buttonTappedCount+1
        if(buttonTappedCount % 2 == 0) {
             imgCredit.isHidden = true
             paymentType = nil
        } else {
            
            imgCredit.isHidden = false
            paymentType = "credit card"
        }
    }
    
    @IBAction func btnBank(_ sender: Any) {
        Alert.showAlertView(withTitle: "", withMessage: "Coming soon")

    }
    
    
    @IBAction func btnMobileMoney(_ sender: Any) {
        Alert.showAlertView(withTitle: "", withMessage: "Coming soon")

    }
    
    @IBAction func btnCash(_ sender: Any) {
        Alert.showAlertView(withTitle: "", withMessage: "Coming soon")

    }
    @IBAction func btnMakePayment(_ sender: Any) {
        if (userID != nil) {
            if (orgID != nil) {
                if (totalAmount != "") && (totalAmount > "0") {
                    let urlString = data.hpplink!.replacingOccurrences(of: "total=xx.xx", with: "total=\(totalAmount!)")
                    if let url = URL(string: urlString) {
                        UIApplication.shared.open(url, options: [:], completionHandler: nil)

                        /*
                        let controller = SFSafariViewController(url: url)
                        self.present(controller, animated: true, completion: {
                            
                        }) */
                    }
                    /*
                    if (self.paymentType != nil) {
                    } else {
                        Alert.showAlertView(withTitle: "Sorry!", withMessage: "Select a Payment Type")
                    } */
                } else {
                    Alert.showAlertView(withTitle: "Sorry!", withMessage: "Please enter amount to proceed")
                }
            } else {
                Alert.showAlertView(withTitle: "Sorry!", withMessage: "Select an organization")
            }
        }
        else {
            //            let uiAlert = UIAlertController(title: "Sorry!", message: "Please register to proceed", preferredStyle: UIAlertController.Style.alert)
            //            self.present(uiAlert, animated: true, completion: nil)
            //
            //            uiAlert.addAction(UIAlertAction(title: "Ok", style: .default, handler: { action in
            //                let VC = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "RegisterViewController") as! RegisterViewController
            //                VC.delegate = self
            //                self.present(VC, animated: true, completion: nil)
            //            }))
            //
            //            uiAlert.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: { action in
            //                self.dismiss(animated: true, completion: nil)
            //            }))
        }
    }
}
//MARK:- TableView dataSourse & delegate methods

extension PaymentMethodViewController: UITableViewDelegate,UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
       
            return flagArray.count
       
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as! CurrencyTableViewCell
        cell.imgFlag.image = flagArray[indexPath.row]
        cell.imgCurrency.image = currencyArray[indexPath.row]

        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let cell = tableView.cellForRow(at: indexPath) as! CurrencyTableViewCell
        imgCurrency.image = currencyArray[indexPath.row]
        imgTopCurrency.image = currencyArray[indexPath.row]

        viewCurrency.isHidden = true
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        let height = tblCurrency.frame.height/5
        return height
    }
    
}
//MARK:- Picker methods
extension PaymentMethodViewController: UIPickerViewDelegate, UIPickerViewDataSource {
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return flagArray.count
    }
    
    func pickerView(_ pickerView: UIPickerView, viewForRow row: Int, forComponent component: Int, reusing view: UIView?) -> UIView {
        
        let myView = UIView()
        myView.frame = CGRect(x: 0, y: 0, width: myPickerView.bounds.width, height: myPickerView.bounds.height)
        let myImageView = UIImageView()
        myImageView.frame = CGRect(x: 10, y: 0, width: 50, height: pickerView.frame.height/5)
         myImageView.image = flagArray[row]
        
        let currencyImgView = UIImageView()
        currencyImgView.frame = CGRect(x: self.view.frame.size.width-26-10, y: 0, width: 26, height: pickerView.frame.height/5)
        currencyImgView.image = currencyArray[row]
        
        myView.addSubview(myImageView)
        myView.addSubview(currencyImgView)
        
        return myView
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        
        imgCurrency.image = currencyArray[row]
        imgTopCurrency.image = currencyArray[row]
        self.view.endEditing(true)
        pickerView.isHidden = true

        
    }
    
    func pickerView(_ pickerView: UIPickerView, rowHeightForComponent component: Int) -> CGFloat {
        return pickerView.frame.height/5
    }
    
}
//MARK:- Api methods

extension PaymentMethodViewController {
    
    func doPaymentapicall(paramDict:[String:Any],Url: String) {
        self.showHUD()
        let url = URL(string: Url)
        var status = ""
        var statusCode = 0
        let queue = DispatchQueue(label: "com.test.api", qos: .background, attributes: .concurrent)
        Alamofire.request(url!, method: .post, parameters: paramDict, encoding: JSONEncoding.default, headers: headers)
            .validate(statusCode: 200..<600)
            .responseObject(queue: queue){ (response: DataResponse<UrlResponse>) in
                DispatchQueue.main.async {
                    guard response.result.isSuccess else {
                        print("Error while fetching remote rooms")
                        print(response.error.debugDescription)
                        status = (response.error?.localizedDescription)!
                        self.dismissHUD()
                        Alert.showAlertView(withTitle: status, withMessage: "")
                        return
                    }
                    self.dismissHUD()
                    let requestResponse = response.result.value
                    let transaction = (requestResponse?.toJSON())
                    print("transaction",transaction)
                }
            }
            .responseObject(queue: queue) { (response: DataResponse<doPaymentResponse>) in
                
                DispatchQueue.main.async {
                    let searchResponse = response.result.value
                    print(searchResponse?.transDic)
                    self.transDic = searchResponse?.transDic
                    print(self.transDic)
                    print("TransId is",self.transDic["id"]!)
                    self.transID = self.transDic["id"] as? String
                    let params: Parameters = [
                        "userid" : self.userID!,
                        "t_date" : self.date!,
                        "amount":  self.totalAmount!,
                        "currency":"usd",
                        "payment_type":self.paymentType!,
                        "payment_mode":self.paymentMode!,
                        "status":"completed",
                        "transaction_id":self.transID!,
                        "transaction_date":self.date!,
                        "notes":self.txtmessage!,
                        "cycle_type":"two",
                        "no_of_cycle":"two",
                        "payment_for":"school",
                        "school_id":self.orgID,
                        "school_account_id":"4",
                        "other_account_name":"nothing",
                        "account_fees":"0",
                        "school_club_id":"3",
                        "club_fees":"0",
                        "response_data":self.transDic!,
                        "response_capture_data":self.transDic!,
                        "created_date": self.date!,
                        "created_by":self.userID!,
                        "last_modified_date":self.date!,
                        "last_modified_by":self.userID!,
                        "resourse_type":"app"
                    ]
                    print(self.orgID!)
                    
                    self.afterPaymentApiCall(paramDict: params, Url: paymentUrl)
                    print(params)
                    
                }
        }
    }
    func callApiClientToken(paramDict:[String:Any],Url: String) {
        self.showHUD()
        let url = URL(string: Url)
        var status = ""
        var statusCode = 0
        let queue = DispatchQueue(label: "com.test.api", qos: .background, attributes: .concurrent)
        Alamofire.request(url!, method: .post, parameters: paramDict, encoding: JSONEncoding.default, headers: headers)
            .validate(statusCode: 200..<600)
            .responseObject(queue: queue){ (response: DataResponse<clientTokenResponse>) in
                DispatchQueue.main.async {
                    guard response.result.isSuccess else {
                        print("Error while fetching remote rooms")
                        print(response.error.debugDescription)
                        status = (response.error?.localizedDescription)!
                        self.dismissHUD()
                        Alert.showAlertView(withTitle: status, withMessage: "")
                        return
                    }
                    self.dismissHUD()
                    let requestResponse = response.result.value
                    status = (requestResponse?.status)!
                    self.clientID = (requestResponse?.token)!
                    print("my clientID is",self.clientID)
                    self.showDropIn(clientTokenOrTokenizationKey: self.clientID!)
                }
        }
    }
    
    func afterPaymentApiCall(paramDict:[String:Any],Url: String) {
        self.showHUD()
        print(paramDict)
        let url = URL(string: Url)
        var status = ""
        var statusCode = 0
        let queue = DispatchQueue(label: "com.test.api", qos: .background, attributes: .concurrent)
        Alamofire.request(url!, method: .post, parameters: paramDict, encoding: JSONEncoding.default, headers: headers)
            .validate(statusCode: 200..<600)
            .responseObject(queue: queue){ (response: DataResponse<URLResponse>) in
                DispatchQueue.main.async {
                    // print(NSString(data: (response.request?.httpBody)!, encoding: String.Encoding.utf8.rawValue))
                    guard response.result.isSuccess else {
                        print("Error while fetching remote rooms")
                        print(response.error.debugDescription)
                        status = (response.error?.localizedDescription)!
                        self.dismissHUD()
                        Alert.showAlertView(withTitle: status, withMessage: "")
                        return
                    }
                    let requestResponse = response.result.value
                    print(requestResponse)
                    status = (requestResponse?.status)!
                    statusCode = (requestResponse?.statusCode)!
                    print(status)
                    print(statusCode)
                    if statusCode == 200  {
                        self.dismissHUD()
                        print(statusCode)
                        print(status)
//                        self.txtPaymntAmt2.text = ""
//                        self.txtPaymntAmt3.text = ""
//                        self.txtDontAmount.text = ""
//                        self.txtSelect.text = ""
//                        self.txtOtherSelect.text = ""
//                        self.btnAmtColor()
//                        self.viewCreditCard.backgroundColor = .white
//                        self.imgTick.isHidden = true
//                        self.imgCard.image = #imageLiteral(resourceName: "cardGreen")
//                        self.lblCreditcard.textColor = UIColor .darkGray
                        self.paymentType = nil
                        self.orgID = nil
                        self.totalAmount = ""
                       
                       // Alert.showAlertView(withTitle: status, withMessage: "Payment Success")
                        let PopUp = popupViewController.init(nibName: "popupViewController", bundle: nil)
                        self.addChild(PopUp)
                        PopUp.view.frame = self.view.frame
                        self.view.addSubview(PopUp.view)
                        PopUp.didMove(toParent: self)
                        
                    } else {
                        if status == "" {
                            self.dismissHUD()
                            let params: Parameters = [
                                "userid" : self.userID!,
                                "t_date" : self.date!,
                                "amount":  self.totalAmount!,
                                "currency":"usd",
                                "payment_type":self.paymentType!,
                                "payment_mode":self.paymentMode!,
                                "status":"completed",
                                "transaction_id":self.transID!,
                                "transaction_date":self.date!,
                                "notes":self.txtmessage!,
                                "cycle_type":"two",
                                "no_of_cycle":"two",
                                "payment_for":"school",
                                "school_id":self.orgID,
                                "school_account_id":"4",
                                "other_account_name":"nothing",
                                "account_fees":"0",
                                "school_club_id":"3",
                                "club_fees":"0",
                                "response_data":self.transDic!,
                                "response_capture_data":self.transDic!,
                                "created_date": self.date!,
                                "created_by":self.userID!,
                                "last_modified_date":self.date!,
                                "last_modified_by":self.userID!,
                                "resourse_type":"app"
                            ]
                            print(self.orgID!)
                            self.afterPaymentApiCall(paramDict: params, Url: paymentUrl)
                        } else {
                            self.dismissHUD()
                            Alert.showAlertView(withTitle: "Something went Wrong", withMessage: status)
                        }
                    }
                }
        }
    }
    

    

    func showHUD() {
        let progressHUD = MBProgressHUD.showAdded(to: self.view, animated: true)
        progressHUD.label.text = "Loading"
    }
    
    func dismissHUD() {
        MBProgressHUD.hide(for: self.view, animated: true)
    }
    
}
//MARK:- Payment Method

extension PaymentMethodViewController: BTDropInControllerDelegate {
    func reloadDropInData() {
        
    }
    
    func editPaymentMethods(_ sender: Any) {
        
    }
    
    
    func showDropIn(clientTokenOrTokenizationKey: String) {
        let request =  BTDropInRequest()
        let dropIn = BTDropInController(authorization: clientTokenOrTokenizationKey, request: request)
        { (controller, result, error) in
            if (error != nil) {
                print("ERROR")
            } else if (result?.isCancelled == true) {
                print("CANCELLED")
            } else if let result = result {
                //print("result is",result.paymentMethod?.nonce)
                self.nounce = result.paymentMethod?.nonce
                let parameters: Parameters = ["nonce" : self.nounce!,"amount" : self.totalAmount!]
                print(parameters)
                self.doPaymentapicall(paramDict: parameters, Url: "http://107.21.25.10/donation/braintree/ckeckout.php")
            }
            controller.dismiss(animated: true, completion: nil)
        }
        self.present(dropIn!, animated: true, completion: nil)
    }
    
}
