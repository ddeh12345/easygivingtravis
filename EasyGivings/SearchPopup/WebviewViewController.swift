

import UIKit
import MBProgressHUD
import WebKit

class WebviewViewController: UIViewController {
    
    @IBOutlet weak var wkwebview: WKWebView!
    
    var youtubeLink: String!
    var selectedId: String!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        if youtubeLink != nil {
            self.showHUD()
            
            let myBlog = youtubeLink
            let url = URL(string: myBlog!)
            self.dismissHUD()
            wkwebview.load(URLRequest(url: url!))
            wkwebview.allowsBackForwardNavigationGestures = true
        }
        
    }
    
    
    @IBAction func btnSkipAction(_ sender: Any) {
        let vc = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "ViewController") as! ViewController
        vc.orgID = selectedId
        self.present(vc, animated: true, completion: nil)
    }
    func showHUD(){
        let progressHUD = MBProgressHUD.showAdded(to: self.view, animated: true)
        progressHUD.label.text = "Loading"
    }
    
    func dismissHUD() {
        MBProgressHUD.hide(for: self.view, animated: true)
    }
}
