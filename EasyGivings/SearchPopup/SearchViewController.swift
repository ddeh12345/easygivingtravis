

import UIKit
import Alamofire
import AlamofireObjectMapper
import MBProgressHUD

protocol Delegate: class {
    func didSelectOrg(_ result: String)
}

class SearchViewController: UIViewController {
    
    @IBOutlet weak var txtSearch: UITextField!
    @IBOutlet weak var tblSearch: UITableView!
    @IBOutlet weak var lblAlert: UILabel!
    var searchListArr = [searchResponse]()
    var delegate: Delegate?
    var selectedId: String!
    var selectedLink: String!
    var label: UILabel!
    var fromHome: Bool!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        selectedId = nil
        tblSearch.register(UINib(nibName: "SearchTableViewCell", bundle: nil), forCellReuseIdentifier: "SearchTableViewCell")
        tblSearch.delegate = self
        tblSearch.dataSource = self
        lblAlert.text = "Your organization is not yet a member"
        lblAlert.isHidden = true
        txtSearch.addTarget(self, action: #selector(self.textFieldDidChange(textField:)), for: UIControl.Event.editingChanged)
        txtSearch.delegate = self
        if 320 >= UIScreen.main.bounds.width {
            txtSearch.font = .systemFont(ofSize: 12)
        }
    }
    
    //MARK:- Button actions
    
    @IBAction func btnSearchTextAction(_ sender: Any) {
        let parameters: Parameters = ["search_key" : txtSearch.text!]
        self.searchapicall(paramDict: parameters,Url: searchApiUrl)
        searchListArr.removeAll()
        self.tblSearch.reloadData()
    }
    
    @IBAction func btnSelectAction(_ sender: Any) {
        
        if (selectedId != nil) {
            if (fromHome != true) {
                
                let vc = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "WebviewViewController") as! WebviewViewController
                
                vc.youtubeLink = selectedLink
                vc.selectedId = selectedId
                self.present(vc, animated: true, completion: nil)
            }
            else {
                removeAnimate()
                fromHome = false
            }
            searchListArr.removeAll()
            tblSearch.reloadData()
            txtSearch.text = ""
        }
        else {
            Alert.showAlertView(withTitle: "Sorry!", withMessage: "Select an organization")
        }
    }
    
    @IBAction func btnCancelAction(_ sender: Any) {
        if (fromHome == true) {
            removeAnimate()
            fromHome = false
        }
        else {
            
        }
        searchListArr.removeAll()
    }
    
    //Remove popup
    func removeAnimate() {
        UIView.animate(withDuration: 0.25, animations: {
            self.view.transform = CGAffineTransform(scaleX: 1.3, y: 1.3)
            self.view.alpha = 0.0
        }, completion: { (finished: Bool) in
            if(finished) {
                self.willMove(toParent: nil)
                self.view.removeFromSuperview()
                self.removeFromParent()
            }
        })
    }
    
}

//MARK:- tableview default methods

extension SearchViewController: UITableViewDelegate,UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return searchListArr.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "SearchTableViewCell", for: indexPath) as! SearchTableViewCell
        if(searchListArr.count != 0) {
            cell.lblName.text = searchListArr[indexPath.row].name
            cell.lblDetail.text = searchListArr[indexPath.row].address
        }
        self.lblAlert.isHidden = true
        return cell
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        selectedId = searchListArr[indexPath.row].id
        selectedLink = searchListArr[indexPath.row].youtubelink
        
        self.delegate?.didSelectOrg(selectedId!)
        
    }
}

//MARK: - api methods

extension SearchViewController {
    func searchapicall(paramDict:[String:Any],Url: String) {
        self.showHUD()
        let url = URL(string: Url)
        var status = ""
        var statusCode = 0
        let queue = DispatchQueue(label: "com.test.api", qos: .background, attributes: .concurrent)
        Alamofire.request(url!, method: .post, parameters: paramDict, encoding: JSONEncoding.default, headers: headers)
            .validate(statusCode: 200..<600)
            .responseObject(queue: queue){ (response: DataResponse<UrlResponse>) in
                DispatchQueue.main.async {
                    guard response.result.isSuccess else {
                        status = (response.error?.localizedDescription)!
                        self.dismissHUD()
                        Alert.showAlertView(withTitle: status, withMessage: "")
                        return
                    }
                    let requestResponse = response.result.value
                    status = (requestResponse?.status)!
                    statusCode = (requestResponse?.statusCode)!
                }
            }
            .responseArray(queue: queue, keyPath: "data") { (response: DataResponse<[searchResponse]>) in
                DispatchQueue.main.async {
                    if statusCode == 200 && status == "Success" {
                        self.searchListArr.removeAll()
                        self.dismissHUD()
                        let searchResponse = response.result.value
                        if let srchResponse = searchResponse {
                            for value in srchResponse {
                                self.searchListArr.append(value)
                            }
                            self.lblAlert.isHidden = true
                            self.tblSearch.reloadData()
                        }
                    }
                    else{
                        if status == "" {
                            self.dismissHUD()
                            let parameters: Parameters = ["search_key" : self.txtSearch.text!]
                            self.searchapicall(paramDict: parameters,Url: searchApiUrl)
                        }
                        else {
                            if(status == "No data found") {
                                self.dismissHUD()
                                self.searchListArr.removeAll()
                                self.tblSearch.reloadData()
                                self.lblAlert.isHidden = false
                            }
                            else if(status != "cancelled") {
                                self.dismissHUD()
                                Alert.showAlertView(withTitle: "", withMessage: status)
                            }
                            else {
                                self.dismissHUD()
                            }
                        }
                    }
                }
        }
    }
    
    func showHUD(){
        let progressHUD = MBProgressHUD.showAdded(to: self.view, animated: true)
        progressHUD.label.text = "Loading"
    }
    
    func dismissHUD() {
        MBProgressHUD.hide(for: self.view, animated: true)
    }
}

//MARK: - TextField Delegate methods

extension SearchViewController: UITextFieldDelegate {
    
    @objc func textFieldDidChange(textField: UITextField) {
        let text = textField.text
        Alamofire.SessionManager.default.session.getTasksWithCompletionHandler { (sessionDataTask, uploadData, downloadData) in
            sessionDataTask.forEach { $0.cancel() }
            uploadData.forEach { $0.cancel() }
            downloadData.forEach { $0.cancel() }
        }
        if let value = text {
            if value != "" {
                let parameters: Parameters = ["search_key" : self.txtSearch.text!]
                self.searchapicall(paramDict: parameters,Url: searchApiUrl)
            }
            else {
                searchListArr.removeAll()
                tblSearch.reloadData()
            }
        }
    }
}
