

import UIKit
import Alamofire
import AlamofireObjectMapper
import MBProgressHUD

protocol Delegate: class {
    func didSelectOrg(_ result: String,_ name: String,_ address: String)
}

class SearchhViewController: UIViewController {

    @IBOutlet weak var btnGoRegister: UIButton!
    @IBOutlet weak var txtSearch: UITextField!
    @IBOutlet weak var tblSearch: UITableView!
    @IBOutlet weak var lblAlert: UILabel!
    var searchListArr = [searchResponse]()
    var orgListArr = [getOrgListResponse]()
    var delegate: Delegate?
    var selectedId: String!
    var selectedLink: String!
    var label: UILabel!
    var fromHome: Bool!
    var name: String!
    var address: String!
    var orgListNameArr: [String] = []
    var filterdItemsArray = [String]()

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        let parameters: Parameters = ["" : ""]
        getOrganizationListapicall(paramDict: parameters, Url: getOrgListApiUrl)
        
        selectedId = nil
        print(selectedId)
       
       // lblAlert.text = "Your organization is not yet a member"
        lblAlert.isHidden = true
        btnGoRegister.isHidden = true
        txtSearch.addTarget(self, action: #selector(self.textFieldDidChange(textField:)), for: UIControl.Event.editingChanged)
        txtSearch.delegate = self
//        txtSearch.attributedPlaceholder = NSAttributedString(string: "Type the name of the organization you want to donate here",
//                                                                       attributes: [NSAttributedString.Key.foregroundColor: UIColor.white])
       
        
//        txtSearch.attributedPlaceholder = NSAttributedString(string: "Type name of the organization you want to donate here", attributes:attributes)
//
        txtSearch.attributedPlaceholder = NSAttributedString(string: "Type name of the organization you want to donate here", attributes: [
            .foregroundColor: UIColor.white,
            .font: UIFont.systemFont(ofSize: 12.0)
            ])
        if 320 >= UIScreen.main.bounds.width {
            //txtSearch.font = .systemFont(ofSize: 10)
            txtSearch.attributedPlaceholder = NSAttributedString(string: "Type name of the organization you want to donate here", attributes: [
                .foregroundColor: UIColor.white,
                .font: UIFont.systemFont(ofSize: 10.0)
                ])
        }
       // let array = [ .... ]
        
    }
    
    @IBAction func btnBack(_ sender: Any) {
       // self.dismiss(animated: true, completion: nil)
        exit(0)
    }
    
    @IBAction func btnHistory(_ sender: Any) {
        let vc = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "TransationHistoryViewController") as! TransationHistoryViewController
        self.present(vc, animated: true, completion: nil)
    }
    @objc func GoRegisterClicked(_ sender: UIButton)  {
        if let url = URL(string: "http://register.easygivings.com") {
            UIApplication.shared.open(url, options: [:])
        }
    }
}
//MARK:- tableview default methods

extension SearchhViewController: UITableViewDelegate,UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
       return searchListArr.count
        //return filterdItemsArray.count
     
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
       
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as! SearchhTableViewCell
       // cell.lblName.text = filterdItemsArray[indexPath.row]
        if(searchListArr.count != 0) {
            cell.lblName.text = searchListArr[indexPath.row].name
            cell.lblDetail.text = searchListArr[indexPath.row].address
        }
        self.lblAlert.isHidden = true
        self.btnGoRegister.isHidden = true
        return cell
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        selectedId = searchListArr[indexPath.row].id
        selectedLink = searchListArr[indexPath.row].youtubelink
        let selectedname = searchListArr[indexPath.row].name
        let selectedaddress = searchListArr[indexPath.row].address

        self.delegate?.didSelectOrg(selectedId!, selectedname!, selectedaddress!)
        
        if (selectedId != nil) {
            if (fromHome != true) {
                
                let vc = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "WebviewViewController") as! WebviewViewController
                vc.data = searchListArr[indexPath.row]
                //vc.orgID = selectedId
                vc.youtubeLink = selectedLink
                vc.selectedId = selectedId
                vc.name = searchListArr[indexPath.row].name
                vc.address = searchListArr[indexPath.row].address
                self.present(vc, animated: true, completion: nil)
            }
            else {
                self.dismiss(animated: true, completion: nil)
                fromHome = false
            }
            searchListArr.removeAll()
            tblSearch.reloadData()
            txtSearch.text = ""
        }
        else {
            Alert.showAlertView(withTitle: "Sorry!", withMessage: "Select an organization")
        }
        
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        let height = tblSearch.frame.height/9
        return height
    }
}

//MARK: - api methods
extension SearchhViewController {
    func searchapicall(paramDict:[String:Any],Url: String) {
        self.showHUD()
        let url = URL(string: Url)
        var status = ""
        var statusCode = 0
        let queue = DispatchQueue(label: "com.test.api", qos: .background, attributes: .concurrent)
        Alamofire.request(url!, method: .post, parameters: paramDict, encoding: JSONEncoding.default, headers: headers)
            .validate(statusCode: 200..<600)
            .responseObject(queue: queue){ (response: DataResponse<UrlResponse>) in
                DispatchQueue.main.async {
                    guard response.result.isSuccess else {
                        print("Error while fetching remote rooms")
                        print(response.error.debugDescription)
                        status = (response.error?.localizedDescription)!
                        self.dismissHUD()
                        //Alert.showAlertView(withTitle: status, withMessage: "")
                        return
                    }
                    let requestResponse = response.result.value
                    status = (requestResponse?.status)!
                    statusCode = (requestResponse?.statusCode)!
                }
            }
            .responseArray(queue: queue, keyPath: "data") { (response: DataResponse<[searchResponse]>) in
                DispatchQueue.main.async {
                    if statusCode == 200 && status == "Success" {
                        self.searchListArr.removeAll()
                        self.dismissHUD()
                        print("Success")
                        let searchResponse = response.result.value
                        print(response.result.value!)
                        if let srchResponse = searchResponse {
                            for value in srchResponse {
                                self.searchListArr.append(value)
                                print(self.searchListArr.toJSON())
                            }
                            self.lblAlert.isHidden = true
                            self.btnGoRegister.isHidden = true
                            self.tblSearch.reloadData()
                        }
                    }
                    else{
                        if status == "" {
                            self.dismissHUD()
                            let parameters: Parameters = ["search_key" : self.txtSearch.text!]
                            self.searchapicall(paramDict: parameters,Url: searchApiUrl)
                        }
                        else {
                            if(status == "No data found") {
                                self.dismissHUD()
                                self.searchListArr.removeAll()
                                self.tblSearch.reloadData()
                                self.lblAlert.isHidden = false
                                self.btnGoRegister.isHidden = false
                                self.btnGoRegister.addTarget(self, action: #selector(self.GoRegisterClicked), for: .touchUpInside)
                            }
                            else if(status != "cancelled") {
                                print(status)
                                self.dismissHUD()
                                Alert.showAlertView(withTitle: "", withMessage: status)
                            } else {
                                self.dismissHUD()
                            }
                        }
                    }
                }
        }
    }
    
    func getOrganizationListapicall(paramDict:[String:Any],Url: String) {
        self.showHUD()
        let url = URL(string: Url)
        var status = ""
        var statusCode = 0
        let queue = DispatchQueue(label: "com.test.api", qos: .background, attributes: .concurrent)
        Alamofire.request(url!, method: .post, parameters: paramDict, encoding: JSONEncoding.default, headers: headers)
            .responseJSON(completionHandler: { (response) in
                // print(response.result.value!)
            })
            .validate(statusCode: 200..<600)
            .responseObject(queue: queue){ (response: DataResponse<UrlResponse>) in
                DispatchQueue.main.async {
                    guard response.result.isSuccess else {
                        print("Error while fetching remote rooms")
                        print(response.error.debugDescription)
                        status = (response.error?.localizedDescription)!
                        self.dismissHUD()
                        Alert.showAlertView(withTitle: status, withMessage: "")
                        return
                    }
                    let requestResponse = response.result.value
                    status = (requestResponse?.status)!
                    statusCode = (requestResponse?.statusCode)!
                }
            }
            .responseArray(queue: queue, keyPath: "data") { (response: DataResponse<[getOrgListResponse]>) in
                DispatchQueue.main.async {
                    if statusCode == 200 && status == "Success" {
                        print("Success")
                        self.dismissHUD()
                        let orgResponse = response.result.value
                        print(response.result.value!)
                        if let orgRes = orgResponse {
                            for value in orgRes {
                                self.orgListArr.append(value)
                                //
                               
                            }
                            //print(self.orgListArr[0].Name)
                           // self.orgListTableView.reloadData()
                            for i in 0 ... self.orgListArr.count-1{
                                self.orgListNameArr.append(self.orgListArr[i].Name!)
                            }
                            print(self.orgListNameArr)
                        }
                        
                    }
                    else {
                        if status == "" {
                            self.dismissHUD()
                            let parameters: Parameters = ["" : ""]
                            self.getOrganizationListapicall(paramDict: parameters, Url: getOrgListApiUrl)
                        } else {
                            self.dismissHUD()
                            Alert.showAlertView(withTitle: "Something went Wrong", withMessage: status)
                        }
                    }
                }
        }
    }
    
    func showHUD(){
        let progressHUD = MBProgressHUD.showAdded(to: self.view, animated: true)
        progressHUD.label.text = "Loading"
    }
    
    func dismissHUD() {
        MBProgressHUD.hide(for: self.view, animated: true)
    }
}
//MARK: - TextField Delegate methods

extension SearchhViewController: UITextFieldDelegate {
    
    @objc func textFieldDidChange(textField: UITextField) {
        let text = textField.text
        Alamofire.SessionManager.default.session.getTasksWithCompletionHandler { (sessionDataTask, uploadData, downloadData) in
            sessionDataTask.forEach { $0.cancel() }
            uploadData.forEach { $0.cancel() }
            downloadData.forEach { $0.cancel() }
        }
        if let value = text {
            if value != "" {
                DispatchQueue.main.async

                 {
                    let parameters: Parameters = ["search_key" : self.txtSearch.text!]
                    self.searchapicall(paramDict: parameters,Url: searchApiUrl)
                }

            }
            else {
                searchListArr.removeAll()
                tblSearch.reloadData()
            }
        }
//        func filterContentForSearchText(searchText: String) {
//            filterdItemsArray = orgListNameArr.filter { item in
//                return item.lowercased().contains(searchText.lowercased())
//            }
//        }
//        filterContentForSearchText(searchText: self.txtSearch.text!)
//        print(filterdItemsArray)
//        tblSearch.reloadData()
    }
}
