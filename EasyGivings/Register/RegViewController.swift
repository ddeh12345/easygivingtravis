

import UIKit

class RegViewController: UIViewController {
    
    @IBOutlet weak var textfieldPhoneno: UITextField!
    @IBOutlet weak var textfieldEmail: UITextField!
    @IBOutlet weak var textfieldLastname: UITextField!
    @IBOutlet weak var textfieldFirstname: UITextField!
    @IBOutlet weak var lblTermsCond: UILabel!
    @IBOutlet weak var viewMobNo: UIView!
    @IBOutlet weak var viewEmailId: UIView!
    @IBOutlet weak var viewLastName: UIView!
    @IBOutlet weak var viewFirstName: UIView!
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        textfieldPhoneno.delegate = self
        textfieldLastname.delegate = self
        textfieldFirstname.delegate = self
        viewFirstName.layer.borderColor = UIColor.lightGray.cgColor
        viewLastName.layer.borderColor = UIColor.lightGray.cgColor
        viewEmailId.layer.borderColor = UIColor.lightGray.cgColor
        viewMobNo.layer.borderColor = UIColor.lightGray.cgColor
        viewMobNo.layer.borderWidth = 1
        viewEmailId.layer.borderWidth = 1
        viewLastName.layer.borderWidth = 1
        viewFirstName.layer.borderWidth = 1
        if 320 >= UIScreen.main.bounds.width {
            //            txtSearch.font = .systemFont(ofSize: 12)
            //lblTermsCond.font = .sys
            lblTermsCond.font = UIFont.systemFont(ofSize: 10.0)
            
        }
    }
    
    @IBAction func btnBack(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    @IBAction func btnSignIn(_ sender: Any) {
        let VC = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "LoginViewController") as! LoginViewController
        self.present(VC, animated: true, completion: nil)
    }
    
    @IBAction func btnNextAction(_ sender: Any) {
        if( !(textfieldFirstname.text?.isEmpty)!){
            if(!(textfieldLastname.text?.isEmpty)!){
                if(!(textfieldEmail.text?.isEmpty)!){
                    if(!(textfieldPhoneno.text?.isEmpty)!){
                        if(textfieldLastname.text!.count>2){
                            if(Validation.isValidEmail(testStr: textfieldEmail.text!)){
                                if(Validation.isMobileValidate(value: textfieldPhoneno.text!)){
                                    let VC = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "RegSecViewController") as! RegSecViewController
                                    VC.firstname = textfieldFirstname.text!
                                    VC.lastName = textfieldLastname.text!
                                    VC.mail = textfieldEmail.text!
                                    VC.phoneno = textfieldPhoneno.text!
                                    self.present(VC, animated: true, completion: nil)
                                }else{
                                    Alert.showAlertView(withTitle:"Sign Up", withMessage: "Invalid Phoneno!")
                                }
                            } else{
                                Alert.showAlertView(withTitle:"Sign Up", withMessage: "Invalid mailID!")
                            }
                        }
                        else{
                            Alert.showAlertView(withTitle:"Last Name", withMessage: "Should be more than one character")
                        }
                    }else{
                        Alert.showAlertView(withTitle:"Sign Up", withMessage: "Phoneno must not be empty")
                    }
                    
                }else{
                    Alert.showAlertView(withTitle:"Sign Up", withMessage: "Email must not be empty")
                }
                
            }else{
                Alert.showAlertView(withTitle:"Sign Up", withMessage: "Last name must not be empty")
            }
        }else{
            Alert.showAlertView(withTitle:"Sign Up", withMessage: "First name must not be empty")
        }
    }
}
//MARK:- textfield Delegate Methods
extension RegViewController: UITextFieldDelegate {
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        if textField == textfieldFirstname || textField == textfieldLastname {
            let allowedCharacters = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz "
            let allowedCharacterSet = CharacterSet(charactersIn: allowedCharacters)
            let typedCharacterSet = CharacterSet(charactersIn: string)
            let alphabet = allowedCharacterSet.isSuperset(of: typedCharacterSet)
            let maxLength = 30
            let currentString: NSString = textField.text! as NSString
            let newString: NSString =
                currentString.replacingCharacters(in: range, with: string) as NSString
            if newString.length <= maxLength {
                return alphabet
            } else {
                return false
            }
        }
        else if textField == textfieldPhoneno {
            let maxLength = 10
            let currentString: NSString = textField.text! as NSString
            let newString: NSString =
                currentString.replacingCharacters(in: range, with: string) as NSString
            return newString.length <= maxLength
        }
        else {
            
            return true
        }
        
        
    }
}
