

import UIKit
import Alamofire
import AlamofireObjectMapper
import MBProgressHUD


class RegSecViewController: UIViewController {
    
    @IBOutlet weak var textfieldconfirmpassword: UITextField!
    @IBOutlet weak var textfieldpassword: UITextField!
    @IBOutlet weak var textfieldusername: UITextField!
    @IBOutlet weak var lblTermsCond: UILabel!
    @IBOutlet weak var viewconfirmPassword: UIView!
    @IBOutlet weak var viewPassword: UIView!
    @IBOutlet weak var viewUsername: UIView!
    var firstname = ""
    var lastName = ""
    var mail = ""
    var phoneno = ""
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        viewconfirmPassword.layer.borderColor = UIColor.lightGray.cgColor
        viewPassword.layer.borderColor = UIColor.lightGray.cgColor
        viewUsername.layer.borderColor = UIColor.lightGray.cgColor
        viewPassword.layer.borderWidth = 1
        viewUsername.layer.borderWidth = 1
        viewconfirmPassword.layer.borderWidth = 1
        if 320 >= UIScreen.main.bounds.width {
            //            txtSearch.font = .systemFont(ofSize: 12)
            //lblTermsCond.font = .sys
            lblTermsCond.font = UIFont.systemFont(ofSize: 10.0)
            
        }
    }
    @IBAction func btnBack(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    @IBAction func btnsignin(_ sender: Any) {
        let VC = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "LoginViewController") as! LoginViewController
        self.present(VC, animated: true, completion: nil)
    }
    @IBAction func btnsignup(_ sender: Any) {
//        guard let userNameText = textfieldusername.text, !userNameText.isEmpty, let passwordText = textfieldpassword.text, !passwordText.isEmpty, let confirmpasswordText = textfieldconfirmpassword.text, !confirmpasswordText.isEmpty else {
//            Alert.showAlertView(withTitle: "Sign Up", withMessage: "Fields should not be empty!")
//            return
//        }
        let passwordText = textfieldpassword.text
        let confirmpasswordText = textfieldconfirmpassword.text
        if( !(textfieldusername.text?.isEmpty)!){
         if( !(textfieldpassword.text?.isEmpty)!){
         if( !(textfieldconfirmpassword.text?.isEmpty)!){
        if (passwordText == confirmpasswordText) {
            let parameters: Parameters = ["name" : firstname,"lname" : lastName,"username" : textfieldusername.text!,"password" : passwordText!,"mobile" : phoneno,"email" : mail,"gender" : "","dob" : "","showdob" : "","aboutme" : "","address" : "","state" : "","city" : "","postcode" : "","country" : "","contactnumber" : ""]
            self.registerApiCall(paramDict: parameters,Url: signupUrl)
        } else {
            Alert.showAlertView(withTitle: "Register", withMessage: "Passwords Mismatch!")
        }
         }
            else {
                Alert.showAlertView(withTitle:"Sign Up", withMessage: "Confirm Password must not be empty")
            }
        }
         else {
            Alert.showAlertView(withTitle:"Sign Up", withMessage: "Password must not be empty")
        }
        }
        else {
             Alert.showAlertView(withTitle:"Sign Up", withMessage: "Username must not be empty")
        }
        
    }
    
    
    
    
}
//MARK:- Api methods

extension RegSecViewController {
    
    func registerApiCall(paramDict:[String:Any],Url: String) {
        self.showHUD()
        let url = URL(string: Url)
        var status = ""
        var statusCode = 0
        let queue = DispatchQueue(label: "com.test.api", qos: .background, attributes: .concurrent)
        Alamofire.request(url!, method: .post, parameters: paramDict, encoding: JSONEncoding.default, headers: headers)
            .validate(statusCode: 200..<600)
            .responseObject(queue: queue){ (response: DataResponse<UrlResponse>) in
                DispatchQueue.main.async {
                    guard response.result.isSuccess else {
                        print("Error while fetching remote rooms")
                        print(response.error.debugDescription)
                        status = (response.error?.localizedDescription)!
                        self.dismissHUD()
                        Alert.showAlertView(withTitle: status, withMessage: "")
                        return
                    }
                    let requestResponse = response.result.value
                    status = (requestResponse?.status)!
                    statusCode = (requestResponse?.statusCode)!
                }
            }
            .responseObject(queue: queue, keyPath: "data") { (response: DataResponse<registerResponse>) in
                DispatchQueue.main.async {
                    if statusCode == 200 && status == "Success"{
                        print("Success")
                        self.dismissHUD()
                        let signupResponse = response.result.value
                        print(response.result.value!)
                        if let id = (signupResponse?.ID)
                        {
                            defaultvalue.set(self.firstname, forKey: "firstname")
                            defaultvalue.set(self.lastName, forKey: "lastname")
                            defaultvalue.set(self.mail, forKey: "email")
                            defaultvalue.set(self.phoneno, forKey: "mobno")
                            // self.delegate?.didSelectData(id, self.FirstName, self.LastName, self.Email, self.MobileNumber)
                            let VC = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "LoginViewController") as! LoginViewController
                            self.present(VC, animated: true, completion:{
                            Alert.showAlertView(withTitle: status, withMessage: "Registered Successfully")
                            })
                            
                            
                        }
                    } else {
                        if status == "" {
                            self.dismissHUD()
                            self.registerApiCall(paramDict: paramDict,Url: signupUrl)
                        } else {
                            self.dismissHUD()
                            Alert.showAlertView(withTitle: "", withMessage: status)
                        }
                    }
                }
        }
    }
    func showHUD() {
        let progressHUD = MBProgressHUD.showAdded(to: self.view, animated: true)
        progressHUD.label.text = "Loading"
    }
    
    func dismissHUD() {
        MBProgressHUD.hide(for: self.view, animated: true)
    }
    
}
