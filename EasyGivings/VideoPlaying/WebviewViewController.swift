

import UIKit
import MBProgressHUD
import WebKit

class WebviewViewController: UIViewController {
    
    @IBOutlet weak var wkwebview: WKWebView!
    
    var data: searchResponse! = nil
    var youtubeLink: String!
    var selectedId: String!
    var name: String!
    var address: String!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
         self.showHUD()
        if youtubeLink != nil {
            //self.showHUD()
            let myBlog = youtubeLink
            self.dismissHUD()
            let url = URL(string: myBlog!)
            wkwebview.load(URLRequest(url: url!))
            wkwebview.allowsBackForwardNavigationGestures = true
            
        } else {
            
            self.dismissHUD()
        }
        
    }
    
    @IBAction func btnBack(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func btnSkipAction(_ sender: Any) {
//        let vc = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "ViewController") as! ViewController
//        vc.orgID = selectedId
//        self.present(vc, animated: true, completion: nil)
        
        let vc = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "NewViewController") as! NewViewController
        vc.data = data
        vc.name = name
        vc.address = address
        vc.orgID = selectedId
        self.present(vc, animated: true, completion: nil)
    }
    func showHUD(){
        let progressHUD = MBProgressHUD.showAdded(to: self.view, animated: true)
        progressHUD.label.text = "Loading"
    }
    
    func dismissHUD() {
        MBProgressHUD.hide(for: self.view, animated: true)
    }
}
