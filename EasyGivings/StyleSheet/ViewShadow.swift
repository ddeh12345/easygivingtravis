

import Foundation
import UIKit

class ViewWithShadow: UIView {
    
    // MARK: Public interface
    /// Corner radius of the background rectangle
    public var roundRectCornerRadius: CGFloat = 10 {
        didSet {
            self.setNeedsLayout()
          
        }
    }
    
    /// Color of the background rectangle
    public var roundRectColor: UIColor = #colorLiteral(red: 0.501960814, green: 0.501960814, blue: 0.501960814, alpha: 1) {
        didSet {
            self.setNeedsLayout()
        }
    }
    
    // MARK: Overrides
    override public func layoutSubviews() {
        super.layoutSubviews()
        //dropShadow()
        dropShadow(color: roundRectColor, opacity: 0.7, offSet: CGSize(width: 0, height: 1), radius: 2, scale: true)
    }
   
    func dropShadow(color: UIColor, opacity: Float = 0.7, offSet: CGSize, radius: CGFloat = 0.0, scale: Bool = true) {
        layer.masksToBounds = false
        layer.shadowColor = roundRectColor.cgColor
        layer.shadowOpacity = opacity
        layer.shadowOffset = offSet
        layer.shadowRadius = radius
        layer.cornerRadius = 5
        layer.shadowPath = UIBezierPath(rect: self.bounds).cgPath
        layer.shouldRasterize = true
        layer.rasterizationScale = scale ? UIScreen.main.scale : 1
    }
    
    
}
class ViewWithLightShadow: UIView {
    
    // MARK: Public interface
    /// Corner radius of the background rectangle
    public var roundRectCornerRadius: CGFloat = 10 {
        didSet {
            self.setNeedsLayout()
            
        }
    }
    
    /// Color of the background rectangle
    public var roundRectColor: UIColor = #colorLiteral(red: 0.501960814, green: 0.501960814, blue: 0.501960814, alpha: 1) {
        didSet {
            self.setNeedsLayout()
        }
    }
    
    // MARK: Overrides
    override public func layoutSubviews() {
        super.layoutSubviews()
        //dropShadow()
        dropShadow(color: roundRectColor, opacity: 0.5, offSet: CGSize(width: 0, height: 1), radius: 2, scale: true)
    }
    
    func dropShadow(color: UIColor, opacity: Float = 0.7, offSet: CGSize, radius: CGFloat = 0.0, scale: Bool = true) {
        layer.masksToBounds = false
        layer.shadowColor = roundRectColor.cgColor
        layer.shadowOpacity = opacity
        layer.shadowOffset = offSet
        layer.shadowRadius = radius
        layer.cornerRadius = 5
        layer.shadowPath = UIBezierPath(rect: self.bounds).cgPath
        layer.shouldRasterize = true
        layer.rasterizationScale = scale ? UIScreen.main.scale : 1
    }
    
    
}
