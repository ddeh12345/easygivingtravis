
import UIKit
import Alamofire
import AlamofireObjectMapper
import MBProgressHUD

class ForgetPasswordViewController: UIViewController {
    
    @IBOutlet weak var txtEmail: UITextField!
    @IBOutlet weak var viewEmail: UIView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        viewEmail.layer.borderColor = UIColor.lightGray.cgColor
        viewEmail.layer.borderWidth = 1
    }
    
    //MARK:- Button Actions
    @IBAction func btnSubmit(_ sender: Any) {
        
        guard let email = txtEmail.text, !email.isEmpty else{
            Alert.showAlertView(withTitle: "", withMessage: "Field should not be empty")
            return
        }
        let parameters: Parameters = ["email" : txtEmail.text!]
        self.frgtpaswrdApiCall(paramDict: parameters,Url: forgetpasswrdUrl)
        
    }
    
    @IBAction func btnBack(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    
}

//MARK: - Api Methods
extension ForgetPasswordViewController {
    
    func frgtpaswrdApiCall(paramDict:[String:Any],Url: String) {
        self.showHUD()
        let url = URL(string: Url)
        var status = ""
        var statusCode = 0
        let queue = DispatchQueue(label: "com.test.api", qos: .background, attributes: .concurrent)
        Alamofire.request(url!, method: .post, parameters: paramDict, encoding: JSONEncoding.default, headers: headers)
            .responseJSON(completionHandler: { (response) in
                print(response.result.value!)
            })
            .validate(statusCode: 200..<600)
            .responseObject(queue: queue){ (response: DataResponse<UrlResponse>) in
                DispatchQueue.main.async {
                    guard response.result.isSuccess else {
                        print("Error while fetching remote rooms")
                        print(response.error.debugDescription)
                        status = (response.error?.localizedDescription)!
                        self.dismissHUD()
                        Alert.showAlertView(withTitle: status, withMessage: "")
                        return
                    }
                    let requestResponse = response.result.value
                    status = (requestResponse?.status)!
                    statusCode = (requestResponse?.statusCode)!
                    
                    if statusCode == 200 && status == "Success"{
                        print("Success")
                        self.dismissHUD()
                        
                        self.dismiss(animated: true, completion: {
                            Alert.showAlertView(withTitle: status, withMessage: "Password reset send successfully")
                        })
                    }
                    else {
                        if status == "" {
                            self.dismissHUD()
                            let parameters: Parameters = ["email" : self.txtEmail.text!]
                            self.frgtpaswrdApiCall(paramDict: parameters,Url: forgetpasswrdUrl)
                        }
                        else {
                            self.dismissHUD()
                            Alert.showAlertView(withTitle: "", withMessage: status)
                        }
                    }
                }
        }
    }
    
    func showHUD() {
        let progressHUD = MBProgressHUD.showAdded(to: self.view, animated: true)
        progressHUD.label.text = "Loading"
    }
    
    func dismissHUD() {
        MBProgressHUD.hide(for: self.view, animated: true)
    }
}
