

import UIKit

class ShareViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    
    @IBAction func btnLogout(_ sender: Any) {
        
        let uiAlert = UIAlertController(title: "Do you want to logout", message: "", preferredStyle: UIAlertController.Style.alert)
        self.present(uiAlert, animated: true, completion: nil)
        uiAlert.addAction(UIAlertAction(title: "Ok", style: .default, handler: { action in
            UserDefaults.standard.set(false, forKey: "isUserLoggedIn")
            UserDefaults.standard.synchronize()
            defaultvalue.removeObject(forKey: "fname")
            defaultvalue.removeObject(forKey: "lname")
            defaultvalue.removeObject(forKey: "email")
            defaultvalue.removeObject(forKey: "mobile")
            defaultvalue.removeObject(forKey: "address")
            defaultvalue.removeObject(forKey: "Message")
            defaultvalue.removeObject(forKey: "Addrss")
            let loginVC = self.storyboard?.instantiateViewController(withIdentifier: "LoginViewController") as! LoginViewController
            let appDel: AppDelegate = UIApplication.shared.delegate as! AppDelegate
            appDel.window?.rootViewController = loginVC
        }))
        uiAlert.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: { action in
        }))
        
    }
    @IBAction func btnGoBack(_ sender: Any) {
        let vc = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "SearchhViewController") as! SearchhViewController
        self.present(vc, animated: true, completion: nil)
        
    }
    @IBAction func btnBack(_ sender: Any) {
        let vc = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "SearchhViewController") as! SearchhViewController
        self.present(vc, animated: true, completion: nil)
        
    }
    @IBAction func btnShare(_ sender: Any) {
        let textToShare = [ shareText ]
        let activityViewController = UIActivityViewController(activityItems: textToShare, applicationActivities: nil)
        activityViewController.popoverPresentationController?.sourceView = self.view
        self.present(activityViewController, animated: true, completion: nil)
    }
    
}
