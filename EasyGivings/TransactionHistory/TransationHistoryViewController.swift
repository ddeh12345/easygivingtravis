
import UIKit
import Alamofire
import AlamofireObjectMapper
import MBProgressHUD

class TransationHistoryViewController: UIViewController {

    @IBOutlet weak var tblHistory: UITableView!
    var historyArr = [historyResponse]()
    var userId: String!

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        if let id = defaultvalue.value(forKey: "ID") as? String {
            userId = id
        }
        let parameters: Parameters = ["userid" : userId!]
        historyapicall(paramDict: parameters, Url: historyApiUrl)
        
    }
    
    @IBAction func btnBack(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
}

//MARK:- tableview default methods
extension TransationHistoryViewController: UITableViewDelegate,UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return historyArr.count
        //return filterdItemsArray.count
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as! TransactionHistoryTableViewCell
        // cell.lblName.text = filterdItemsArray[indexPath.row]
       
            cell.lblOrgName.text = historyArr[indexPath.row].organization
            cell.lblTranstnId.text = historyArr[indexPath.row].transactionid
            cell.lblDate.text = historyArr[indexPath.row].transactiondate
            cell.amount.setTitle("$ " + historyArr[indexPath.row].amount!, for: .normal)

            return cell
    }
   
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        let height = tblHistory.frame.height/5
        return height
    }
}

//MARK: - api methods

extension TransationHistoryViewController {
    func historyapicall(paramDict:[String:Any],Url: String) {
        print(paramDict)
        self.showHUD()
        let url = URL(string: Url)
        var status = ""
        var statusCode = 0
        let queue = DispatchQueue(label: "com.test.api", qos: .background, attributes: .concurrent)
        Alamofire.request(url!, method: .post, parameters: paramDict, encoding: JSONEncoding.default, headers: headers)
            .validate(statusCode: 200..<600)
            .responseObject(queue: queue){ (response: DataResponse<URLResponse>) in
                DispatchQueue.main.async {
                    guard response.result.isSuccess else {
                        print("Error while fetching remote rooms")
                        print(response.error.debugDescription)
                        status = (response.error?.localizedDescription)!
                        self.dismissHUD()
                        //Alert.showAlertView(withTitle: status, withMessage: "")
                        return
                    }
                    let requestResponse = response.result.value
                    status = (requestResponse?.status)!
                    statusCode = (requestResponse?.statusCode)!
                }
            }
            .responseArray(queue: queue, keyPath: "data") { (response: DataResponse<[historyResponse]>) in
                DispatchQueue.main.async {
                    if statusCode == 200 && status == "success" {
                        //self.searchListArr.removeAll()
                        self.dismissHUD()
                        print("Success")
                        let historyResponse = response.result.value
                        print(response.result.value!)
                        if let apiResponse = historyResponse {
                            for value in apiResponse {
                                self.historyArr.append(value)
                                print(self.historyArr.toJSON())
                            }
                            
                            self.tblHistory.reloadData()
                        }
                    }
                    else {
                        if status == "" {
                            self.dismissHUD()
                            self.historyapicall(paramDict: paramDict, Url: Url)
                        } else {
                            self.dismissHUD()
                            Alert.showAlertView(withTitle: "Something went Wrong", withMessage: status)
                        }
                    }
                }
        }
    }
    
    func showHUD(){
        let progressHUD = MBProgressHUD.showAdded(to: self.view, animated: true)
        progressHUD.label.text = "Loading"
    }
    
    func dismissHUD() {
        MBProgressHUD.hide(for: self.view, animated: true)
    }
}
