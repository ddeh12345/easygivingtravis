
import UIKit
import Alamofire
import AlamofireObjectMapper
import MBProgressHUD
//import GoogleSignIn
//import FacebookCore
//import FacebookLogin
//import FBSDKLoginKit

class LoginViewController: UIViewController {
    
    @IBOutlet weak var viewPassword: UIView!
    @IBOutlet weak var viewUsername: UIView!
    @IBOutlet weak var txtPassword: UITextField!
    @IBOutlet weak var txtUsername: UITextField!
    var fname: String!
    var lname: String!
    var emailid: String!
    var mobile: String!
    var address: String!
    var dict : [String : AnyObject]!
    var dictionary = NSDictionary()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        viewPassword.layer.borderColor = UIColor.lightGray.cgColor
        viewPassword.layer.borderWidth = 1
        viewUsername.layer.borderColor = UIColor.lightGray.cgColor
        viewUsername.layer.borderWidth = 1
    }
    //    override var preferredStatusBarStyle: UIStatusBarStyle {
    //        return .lightContent
    //    }
    
    //MARK:- Button Actions
    @IBAction func btnFbSignIn(_ sender: Any) {
        //       signinOption = 1
        //        let loginManager = LoginManager()
        //        loginManager.logOut()
        //        loginManager.logIn(readPermissions: [.publicProfile, .email], viewController: self) { loginResult in
        //            switch loginResult {
        //            case .failed(let error):
        //                print(error)
        //            case .cancelled:
        //                print("User cancelled login.")
        //            case .success(let grantedPermissions, let declinedPermissions, let accessToken):
        //                self.getFBUserData()
        //                print("Logged in! \(grantedPermissions) \(declinedPermissions) \(accessToken)")
        //                loginManager.logOut()
        //
        //            }
        //
        //        }
        
        Alert.showAlertView(withTitle: "Coming Soon", withMessage: "")
    }
    
    @IBAction func btnGmailSignIn(_ sender: Any) {
        //        signinOption = 0
        //        GIDSignIn.sharedInstance().delegate = self as? GIDSignInDelegate
        //        GIDSignIn.sharedInstance().uiDelegate = self as? GIDSignInUIDelegate
        //        GIDSignIn.sharedInstance().signIn()
        
        Alert.showAlertView(withTitle: "Coming Soon", withMessage: "")
    }
    
    @IBAction func btnTwitterLogin(_ sender: Any) {
        Alert.showAlertView(withTitle: "Coming Soon", withMessage: "")
    }
    
    @IBAction func btnLinkedInLogin(_ sender: Any) {
        Alert.showAlertView(withTitle: "Coming Soon", withMessage: "")
    }
    
    @IBAction func btnGithhubLogin(_ sender: Any) {
        Alert.showAlertView(withTitle: "Coming Soon", withMessage: "")
    }
    
    @IBAction func btnRegister(_ sender: Any) {
        //        let VC = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "RegisterViewController") as! RegisterViewController
        //        self.present(VC, animated: true, completion: nil)
        let VC = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "RegViewController") as! RegViewController
        self.present(VC, animated: true, completion: nil)
        
    }
    
    @IBAction func btnLogin(_ sender: Any) {
        let name = txtUsername.text
        let password = txtPassword.text
        if( !(txtUsername.text?.isEmpty)!){
            if( !(txtPassword.text?.isEmpty)!){
                let parameters: Parameters = ["username" : name,"password" : password]
                self.loginApiCall(paramDict: parameters, Url: loginUrl)
            }
            else {
                Alert.showAlertView(withTitle:"Login", withMessage: "Password should not be empty")
            }
        } else {
            Alert.showAlertView(withTitle:"Login", withMessage: "Username should not be empty")
        }
    }
    
    @IBAction func btnForgetPswrd(_ sender: Any) {
        
        let vc = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "ForgetPasswordViewController") as! ForgetPasswordViewController
        self.present(vc, animated: true, completion: nil)
        
    }
    //MARK:- Google Delegate
//    func sign(_ signIn: GIDSignIn!, present viewController: UIViewController)
//    {
//        self.present(viewController, animated: true, completion: nil)
//    }
//    func sign(_ signIn: GIDSignIn!, dismiss viewController: UIViewController)
//    {
//        self.dismiss(animated: true, completion: nil)
//    }
//
//    func sign(_ signIn: GIDSignIn!, didSignInFor user: GIDGoogleUser!, withError error: Error!) {
//        if let error = error {
//            print("error\(error)")
//        }
//        else
//        {
//            let loginId = user.userID
//            let firstName = user.profile.givenName
//            let lastName = user.profile.familyName
//            let email = user.profile.email
//            print(firstName!)
//            print(loginId!)
//            print(email!)
//
//
//        }
//    }
//
//    //MARK:- Facebook
//    func getFBUserData(){
//
//        if((FBSDKAccessToken.current()) != nil){
//            FBSDKGraphRequest(graphPath: "me", parameters: ["fields": "id, name, first_name, last_name, email"]).start(completionHandler: { (connection, result, error) -> Void in
//                if (error == nil)
//                {
//                    self.dict = result as? [String : AnyObject]
//                    print(self.dict)
//                    self.dictionary = result as! NSDictionary
//                    print("Id :\(self.dictionary.object(forKey: "id")  as! String)")
//                    print("name :\(self.dictionary.object(forKey: "name")  as! String)")
//                    let tempdict = self.dictionary.object(forKey: "picture") as! NSDictionary
//                    let dpDict = tempdict.object(forKey: "data") as! NSDictionary
//                    print("Id :\(dpDict.object(forKey: "url")  as! String)")
//
//                }
//            })
//        }
//    }
}

//MARK:- Api methods

extension LoginViewController {
    func loginApiCall(paramDict:[String:Any],Url: String) {
        self.showHUD()
        let url = URL(string: Url)
        var status = ""
        var statusCode = 0
        let queue = DispatchQueue(label: "com.test.api", qos: .background, attributes: .concurrent)
        Alamofire.request(url!, method: .post, parameters: paramDict, encoding: JSONEncoding.default, headers: headers)
            .validate(statusCode: 200..<600)
            .responseObject(queue: queue){ (response: DataResponse<UrlResponse>) in
                DispatchQueue.main.async {
                    guard response.result.isSuccess else {
                        print("Error while fetching remote rooms")
                        print(response.error.debugDescription)
                        status = (response.error?.localizedDescription)!
                        self.dismissHUD()
                        Alert.showAlertView(withTitle: status, withMessage: "")
                        return
                    }
                    let requestResponse = response.result.value
                    status = (requestResponse?.status)!
                    statusCode = (requestResponse?.statusCode)!
                }
            }
            .responseObject(queue: queue, keyPath: "data") { (response: DataResponse<loginResponse>) in
                DispatchQueue.main.async {
                    if statusCode == 200 && status == "Success"{
                        print("Success")
                        self.dismissHUD()
                        let loginResponse = response.result.value
                        print(response.result.value!)
                        if let id = (loginResponse?.userID)
                        {
                            defaultvalue.set(id, forKey: "ID")
                            print(id)
                            
                            self.fname = loginResponse?.fname
                            self.lname = loginResponse?.lname
                            self.emailid = loginResponse?.email
                            self.mobile = loginResponse?.mobile
                            self.address = loginResponse?.address
                            
                            print(self.fname!)
                            defaultvalue.set(self.fname!, forKey: "fname")
                            defaultvalue.set(self.lname!, forKey: "lname")
                            defaultvalue.set(self.emailid!, forKey: "email")
                            defaultvalue.set(self.mobile!, forKey: "mobile")
                            defaultvalue.set(self.address!, forKey: "address")
                            
                        }
                        //                        let searchpopup = SearchhViewController.init(nibName: "SearchhViewController", bundle: nil)
                        //                        self.present(searchpopup, animated: true, completion: nil)
                        UserDefaults.standard.set(true, forKey: "isUserLoggedIn")
                        UserDefaults.standard.synchronize()
                        let vc = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "SearchhViewController") as! SearchhViewController
                        self.present(vc, animated: true, completion: nil)
                    }
                    else {
                        if status == "" {
                            self.dismissHUD()
                            let parameters: Parameters = ["username" : self.txtUsername.text!,"password" : self.txtPassword.text!]
                            self.loginApiCall(paramDict: parameters, Url: loginUrl)
                        }
                        else {
                            self.dismissHUD()
                            Alert.showAlertView(withTitle: "", withMessage: status)
                        }
                    }
                }
        }
    }
    func SocialMediaApiCall(paramDict:[String:Any],Url: String) {
        self.showHUD()
        let url = URL(string: Url)
        var status = ""
        var statusCode = 0
        let queue = DispatchQueue(label: "com.test.api", qos: .background, attributes: .concurrent)
        Alamofire.request(url!, method: .post, parameters: paramDict, encoding: JSONEncoding.default, headers: headers)
            .validate(statusCode: 200..<600)
            .responseObject(queue: queue){ (response: DataResponse<UrlResponse>) in
                DispatchQueue.main.async {
                    guard response.result.isSuccess else {
                        print("Error while fetching remote rooms")
                        print(response.error.debugDescription)
                        status = (response.error?.localizedDescription)!
                        self.dismissHUD()
                        Alert.showAlertView(withTitle: status, withMessage: "")
                        return
                    }
                    let requestResponse = response.result.value
                    status = (requestResponse?.status)!
                    statusCode = (requestResponse?.statusCode)!
                }
            }
            .responseObject(queue: queue, keyPath: "data") { (response: DataResponse<socialmediaResponse>) in
                DispatchQueue.main.async {
                    if statusCode == 200 && status == "Success"{
                        print("Success")
                        self.dismissHUD()
                        let apiResponse = response.result.value
                        print(response.result.value!)
                        if let id = (apiResponse?.userID)
                        {
                            defaultvalue.set(id, forKey: "ID")
                            print(id)
                            
                            self.fname = apiResponse?.fname
                            self.lname = apiResponse?.lname
                            self.emailid = apiResponse?.email
                            self.mobile = apiResponse?.mobile
                            self.address = apiResponse?.address
                            
                            print(self.fname!)
                            defaultvalue.set(self.fname!, forKey: "fname")
                            defaultvalue.set(self.lname!, forKey: "lname")
                            defaultvalue.set(self.emailid!, forKey: "email")
                            defaultvalue.set(self.mobile!, forKey: "mobile")
                            defaultvalue.set(self.address!, forKey: "address")
                            
                        }
                        UserDefaults.standard.set(true, forKey: "isUserLoggedIn")
                        UserDefaults.standard.synchronize()
                        let vc = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "SearchhViewController") as! SearchhViewController
                        self.present(vc, animated: true, completion: nil)
                    }
                    else {
                        if status == "" {
                            self.dismissHUD()
                            
                            self.SocialMediaApiCall(paramDict: paramDict, Url: Url)
                        }
                        else {
                            self.dismissHUD()
                            Alert.showAlertView(withTitle: "", withMessage: status)
                        }
                    }
                }
        }
    }
    func showHUD(){
        let progressHUD = MBProgressHUD.showAdded(to: self.view, animated: true)
        progressHUD.label.text = "Loading"
    }
    
    func dismissHUD() {
        MBProgressHUD.hide(for: self.view, animated: true)
    }
}
