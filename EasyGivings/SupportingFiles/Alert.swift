import Foundation
import UIKit

let generalUtils = GenerelUitilitis()

//let defaults = UserDefaults.standard
open class Alert: NSObject {
    
    class func showAlertView(withTitle title: String, withMessage message: String) -> Void {
        
        let alertViewController = UIAlertController(title: title, message: message, preferredStyle: .alert)
        let okAction = UIAlertAction(title: "OK", style: .cancel, handler: nil)
        alertViewController.addAction(okAction)
        generalUtils.topMostController().present(alertViewController, animated: true, completion: nil)
    }
    
    
    
    
}
