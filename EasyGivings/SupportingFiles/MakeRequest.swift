

import Foundation
//import SwiftyJSON

open class MakeRequest
{

class func makeMyRequest(endPoint: String, apiParameters: String) -> URLRequest {
    
    var request = URLRequest(url: URL(string: Base_Path+endPoint)!)
    request.httpMethod = "POST"
    request.setValue(Api_Key, forHTTPHeaderField: "apiKey")
    request.addValue("application/json", forHTTPHeaderField: "Accept")
    
    request.httpBody = apiParameters.data(using: String.Encoding(rawValue: String.Encoding.utf8.rawValue))!
    return request
}
    
    class func makemyAuthRequest(endPoint: String, apiParameters: String) -> URLRequest {
        
        var request = URLRequest(url: URL(string: "https://connect.squareup.com/mobile/"+endPoint)!)
        request.httpMethod = "POST"
        request.setValue("Bearer sq0atp-Tqw4tR__lnWXwyAILzp9dA", forHTTPHeaderField: "Authorization")
        request.addValue("application/json", forHTTPHeaderField: "Content-Type")
        
        request.httpBody = apiParameters.data(using: String.Encoding(rawValue: String.Encoding.utf8.rawValue))!
        return request
    }

}
