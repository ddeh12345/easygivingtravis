import Foundation
import ObjectMapper

class UrlResponse: Mappable {
    var status: String?
    var statusCode: Int?
    
    required init?(map: Map){
        
    }
    
    func mapping(map: Map) {
        status                      <- map["status"]
        statusCode                  <- map["statusCode"]
    }
}

class getOrgListResponse: Mappable{
    
    var Id: String?
    var Name: String?
   
    required init?(map: Map) {
        
    }
    
    func mapping(map: Map) {
        
        Id                          <- map["id"]
        Name                          <- map["name"]

    }
}
class getOrgDetailResponse: Mappable{
    
    var Id: String?
    var Name: String?
    var Club: Array<Any>?
    

    required init?(map: Map) {
        
    }
    
    func mapping(map: Map) {
        
        Id                          <- map["id"]
        Name                          <- map["name"]
        Club                            <- map["clubs"]
        
    }
}
class searchResponse: Mappable{
    
    var name: String?
    var address: String?
    var id: String?
    var youtubelink: String?
    var hpplink: String?

    required init?(map: Map) {
        
    }
    
    func mapping(map: Map) {
        
        name                          <- map["name"]
        address                       <- map["address"]
        id                            <- map["id"]
        youtubelink                   <- map["youTube"]
        hpplink                       <- map["hpplink"]

    }
}

class clientTokenResponse: Mappable{
    
    var token: String?
    var status: String?
    var statusCode: Int?
    
    
    required init?(map: Map) {
        
    }
    
    func mapping(map: Map) {
        
        token                          <- map["clientToken"]
        status                         <- map["status"]
        statusCode                     <- map["code"]
        
    }
}
class doPaymentResponse: Mappable{
    
   
    var transDic: [String : AnyObject]?
    var transID: String?
    
    required init?(map: Map) {
        
    }
    
    func mapping(map: Map) {
        
       
     
        transDic                         <- map["transaction"]
        transID                          <- map["id"]
        
    }
}

class registerResponse: Mappable{
    
    var ID: String?
    
    
    required init?(map: Map) {
        
    }
    
    func mapping(map: Map) {
       
        ID                       <- map["userid"]
        
    }
}

class URLResponse: Mappable{
    
    var status: String?
    var statusCode: Int?
   
    required init?(map: Map) {
        
    }
    
    func mapping(map: Map) {
        
        status                       <- map["status"]
        statusCode                   <- map["statuscode"]

        
    }
}


class loginResponse: Mappable{
    
    var userID: String?
    var email: String?
    var name: String?
    var fname: String?
    var lname: String?
    var mobile: String?
    var address: String?

    required init?(map: Map) {
        
    }
    
    func mapping(map: Map) {
        
        userID                       <- map["userid"]
        email                       <- map["email"]
        name                       <- map["name"]
        fname                       <- map["fname"]
        lname                       <- map["lname"]
        mobile                       <- map["mobile"]
        address                       <- map["address"]

    }
}

class historyResponse: Mappable{
    
    var transactionid: String?
    var transactiondate: String?
    var amount: String?
    var organization: String?

    
    required init?(map: Map) {
        
    }
    
    func mapping(map: Map) {
        
        transactionid                       <- map["t_id"]
        transactiondate                     <- map["t_date"]
        amount                              <- map["amount"]
        organization                        <- map["organisation"]

        
    }
}
class socialmediaResponse: Mappable{
    
    var userID: String?
    var email: String?
    var name: String?
    var fname: String?
    var lname: String?
    var mobile: String?
    var address: String?
    
    required init?(map: Map) {
        
    }
    
    func mapping(map: Map) {
        
        userID                       <- map["userid"]
        email                       <- map["email"]
        name                       <- map["name"]
        fname                       <- map["fname"]
        lname                       <- map["lname"]
        mobile                       <- map["mobile"]
        address                       <- map["address"]
        
    }
}
