

import UIKit

protocol msgDelegate: class {
    func message(_ message: String)
}
class PersonalDetailsViewController: UIViewController {

    @IBOutlet weak var viewTextView: UIView!
    @IBOutlet weak var txtTextView: UITextView!
    @IBOutlet weak var txtAddress: UITextField!
    @IBOutlet weak var txtMobile: UITextField!
    @IBOutlet weak var txtEmail: UITextField!
    @IBOutlet weak var txtName: UITextField!
    
    var delegate: msgDelegate?

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        viewTextView.layer.borderColor = #colorLiteral(red: 0.8039215803, green: 0.8039215803, blue: 0.8039215803, alpha: 0.4387842466)
        viewTextView.layer.borderWidth = 1
        if let name = defaultvalue.value(forKey: "fname") as? String {
            //userID = id
            txtName.text = name
            txtAddress.text = defaultvalue.value(forKey: "address") as? String
            txtMobile.text = defaultvalue.value(forKey: "mobile") as? String
            txtEmail.text = defaultvalue.value(forKey: "email") as? String
        }
        txtTextView.text = "Enter any message"
        txtTextView.textColor = UIColor.lightGray
        txtTextView.delegate = self
        txtAddress.text = "Address"
        txtAddress.textColor = UIColor.lightGray
        txtAddress.delegate = self
        if let msg = defaultvalue.value(forKey: "Message") {
            txtTextView.textColor = UIColor.black
            txtTextView.text = msg as? String
        }
        if let addr = defaultvalue.value(forKey: "Addrss") {
            txtAddress.textColor = UIColor.black
            txtAddress.text = addr as? String
        }
    }
    
    @IBAction func btnBack(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
//    override var preferredStatusBarStyle: UIStatusBarStyle {
//        return .lightContent
//    }
    

}
//MARK:- textview Delegate Methods
extension PersonalDetailsViewController: UITextViewDelegate {
    func textViewDidBeginEditing(_ textView: UITextView) {
        if textView.textColor == UIColor.lightGray {
            textView.text = nil
            textView.textColor = UIColor.black
        }
    }
    func textViewDidEndEditing(_ textView: UITextView) {
       
        if textView.text.isEmpty {
            defaultvalue.removeObject(forKey: "Message")
            textView.text = "Enter any message"
            textView.textColor = UIColor.lightGray
        } else {
            self.delegate?.message(textView.text)
            defaultvalue.set(textView.text, forKey: "Message")

        }
      //  if txtAddress.text!.isEmpty {
        
        
    }

}
//MARK:- textfield Delegate Methods
extension PersonalDetailsViewController: UITextFieldDelegate {
    func textFieldDidBeginEditing(_ textField: UITextField) {
        if txtAddress.textColor == UIColor.lightGray {
            txtAddress.text = nil
            txtAddress.textColor = UIColor.black
        }
    }
    func textFieldDidEndEditing(_ textField: UITextField) {
        if txtAddress.text!.isEmpty {
            defaultvalue.removeObject(forKey: "Addrss")
            txtAddress.text = "Address"
            txtAddress.textColor = UIColor.lightGray
        } else {
            defaultvalue.set(txtAddress.text, forKey: "Addrss")
            
        }
    }
}
